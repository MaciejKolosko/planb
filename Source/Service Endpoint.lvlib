﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5Q&lt;/07RB7W!,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@PWW`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"\Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"XC-_N!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Abstract" Type="Folder">
		<Item Name="IReader Messages" Type="Folder">
			<Item Name="Connect Msg.lvclass" Type="LVClass" URL="../Classes/IReader Messages/Connect Msg/Connect Msg.lvclass"/>
		</Item>
		<Item Name="IWriter Messages" Type="Folder">
			<Item Name="Bind Msg.lvclass" Type="LVClass" URL="../Classes/IWriter Messages/Bind Msg/Bind Msg.lvclass"/>
		</Item>
		<Item Name="IEndpoint Messages" Type="Folder">
			<Item Name="Recieve Msg.lvclass" Type="LVClass" URL="../Classes/IEndpoint Messages/Recieve Msg/Recieve Msg.lvclass"/>
			<Item Name="Send Msg.lvclass" Type="LVClass" URL="../Classes/IEndpoint Messages/Send Msg/Send Msg.lvclass"/>
		</Item>
		<Item Name="IEndpoint.lvclass" Type="LVClass" URL="../Classes/IEndpoint/IEndpoint.lvclass"/>
		<Item Name="IReader.lvclass" Type="LVClass" URL="../Classes/IReader/IReader.lvclass"/>
		<Item Name="IWriter.lvclass" Type="LVClass" URL="../Classes/IWriter/IWriter.lvclass"/>
	</Item>
	<Item Name="Concrete" Type="Folder">
		<Item Name="Readers" Type="Folder">
			<Item Name="Requester.lvclass" Type="LVClass" URL="../Classes/Requester/Requester.lvclass"/>
			<Item Name="Subscriber.lvclass" Type="LVClass" URL="../Classes/Subscriber/Subscriber.lvclass"/>
		</Item>
		<Item Name="Writers" Type="Folder">
			<Item Name="Publisher.lvclass" Type="LVClass" URL="../Classes/Publisher/Publisher.lvclass"/>
			<Item Name="Responder With Proxy.lvclass" Type="LVClass" URL="../Classes/Responder with Proxy/Responder With Proxy.lvclass"/>
		</Item>
	</Item>
	<Item Name="Create Reader.vi" Type="VI" URL="../VIs/Create Reader.vi"/>
	<Item Name="Create Writer.vi" Type="VI" URL="../VIs/Create Writer.vi"/>
	<Item Name="Sevice.ctl" Type="VI" URL="../Controls/Sevice.ctl"/>
	<Item Name="Define Endpoint Compatibility.vi" Type="VI" URL="../VIs/Define Endpoint Compatibility.vi"/>
</Library>
